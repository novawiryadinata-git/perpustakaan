<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TabelBukuController extends Controller
{
    public function read()
    {
        $buku = DB::table('buku')->get();
        return view('tabelbuku',['buku' => $buku]);
    }
    public function addData()
    {
        return view('tambahbuku');
    }
    public function create(Request $request)
    {
        DB::table('buku')->insert([
            'judul_buku' => $request->judul_buku,
            'pengarang' => $request->pengarang,
            'penerbit' => $request->penerbit,
            'tahun_terbit' => $request->tahun_terbit,
            'tebal' => $request->tebal,
            'isbn' => $request->isbn,
            'stok_buku' => $request->stok_buku,
            'biaya_sewa_harian' => $request->biaya_sewa_harian,
        ]);
        return redirect('/buku');
    }

    public function delete($id){
        DB::table('buku')->where('id', $id)->delete();

        return redirect('/buku');
    }

    public function edit($id)
    {
        $buku = DB::table('buku')-> where('id', $id)->get();
        return view('/editbuku',['buku'=> $buku]);
    }

    public function update(Request $request)
    {
        DB::table('buku')->where('id', $request->id)->update([
            'judul_buku' => $request->judul_buku,
            'pengarang' => $request->pengarang,
            'penerbit' => $request->penerbit,
            'tahun_terbit' => $request->tahun_terbit,
            'tebal' => $request->tebal,
            'isbn' => $request->isbn,
            'stok_buku' => $request->stok_buku,
            'biaya_sewa_harian' => $request->biaya_sewa_harian,
        ]);

        return redirect('/buku');
    }
}
