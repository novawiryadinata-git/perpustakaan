<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TabelMahasiswaController extends Controller
{
    //Read
    public function read()
    {
        //mengambil data dari tabel mahasiswa
        $mahasiswa = DB::table('mahasiswa')->get();
        //mengirim data ke view mahasiswa
        return view('/tabelmahasiswa', ['mahasiswa' => $mahasiswa]);
    }
    public function addData()
    {
        return view('/tambahmahasiswa');
    }

    public function create(Request $request)
    {
        DB::table('mahasiswa')->insert([
            'nama' => $request->nama,
            'nim' => $request->nim,
            'email' => $request->email,
            'no_telp' => $request->no_telp,
            'prodi' => $request->prodi,
            'jurusan' => $request->jurusan,
            'fakultas' => $request->fakultas,
        ]);
        return redirect('/home');
    }

    public function delete($id){
        DB::table('mahasiswa')->where('id', $id)->delete();

        return redirect('/home');
    }

    public function edit($id)
    {
        $mahasiswa = DB::table('mahasiswa')-> where('id', $id)->get();
        return view('/editmahasiswa',['mahasiswa'=> $mahasiswa]);
    }

    public function update(Request $request)
    {
        DB::table('mahasiswa')->where('id', $request->id)->update([
            'nama' => $request->nama,
            'nim' => $request->nim,
            'email' => $request->email,
            'no_telp' => $request->no_telp,
            'prodi' => $request->prodi,
            'jurusan' => $request->jurusan,
            'fakultas' => $request->fakultas,
        ]);

        return redirect('/home');
    }




}
