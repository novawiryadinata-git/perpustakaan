<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/home', function () {
//     return view('tabelmahasiswa');
// });

Route::get('/home', [App\Http\Controllers\TabelMahasiswaController::class,'read']);
Route::get('/home/create', [App\Http\Controllers\TabelMahasiswaController::class,'addData']);
Route::post('/home/simpan', [App\Http\Controllers\TabelMahasiswaController::class,'create']);
Route::get('/home/delete/{id}',[App\Http\Controllers\TabelMahasiswaController::class,'delete']);
Route::get('/home/editmahasiswa/{id}', [App\Http\Controllers\TabelMahasiswaController::class,'edit']);
Route::post('/home/update',[App\Http\Controllers\TabelMahasiswaController::class,'update']);

Route::get('/buku', [App\Http\Controllers\TabelBukuController::class,'read']);
Route::get('/buku/create', [App\Http\Controllers\TabelBukuController::class,'addData']);
Route::post('/buku/simpan', [App\Http\Controllers\TabelBukuController::class,'create']);
Route::get('/buku/delete/{id}',[App\Http\Controllers\TabelBukuController::class,'delete']);
Route::get('/buku/editbuku/{id}', [App\Http\Controllers\TabelBukuController::class,'edit']);
Route::post('/buku/update',[App\Http\Controllers\TabelBukuController::class,'update']);

Route::get('/transaksi', [App\Http\Controllers\TabelTransaksiController::class,'read'])->name('transaksi.index');
Route::get('/transaksi/create', [App\Http\Controllers\TabelTransaksiController::class,'addData']);
Route::post('/transaksi/simpan', [App\Http\Controllers\TabelTransaksiController::class,'create']);
Route::get('/transaksi/delete/{id}',[App\Http\Controllers\TabelTransaksiController::class,'delete']);
Route::get('/transaksi/update/{id}', [App\Http\Controllers\TabelTransaksiController::class,'update']);
Route::post('/transaksi/updating',[App\Http\Controllers\TabelTransaksiController::class,'updating']);
Route::post('/transaksi/updating-status',[App\Http\Controllers\TabelTransaksiController::class,'updating_status']);

// Route::get('/transaksi', function () {
//     return view('tabeltransaksi');
// });

// Route::get('/buku', function () {
//     return view('tabelbuku');
// });


// Route::get('/tambahmahasiswa', function () {
//     return view('tambahmahasiswa');
// });


