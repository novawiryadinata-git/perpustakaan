@extends('layout2.template')
@section('title','Tambah Data Buku')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="card shadow-lg border-0 rounded-lg mt-5">
                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Tambah Transaksi</h3></div>
                    <div class="card-body">
                        <form action="/transaksi/simpan" method="POST">
                            {{ csrf_field() }}
                            <div class="form-floating mb-3">
                                <select name="mahasiswa" id="mahasiswa"  class="selectpicker form-control" data-live-search="true" type="text" placeholder="Nama Mahasiswa" required>
                                    <option value="">Pilih Mahasiswa</option>
                                    @foreach ($mahasiswa as $mhs)
                                        <option value="{{$mhs->id}} "> {{$mhs->nama}} </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-floating mb-3">
                                <select name="buku" id="buku" class="selectpicker form-control" data-live-search="true" type="text" placeholder="Judul Buku" required>
                                    <option value="">Pilih Buku</option>
                                    @foreach ($buku as $buku )
                                        @if ($buku->stok_buku <= 0)
                                            <option disabled>{{$buku->stok_buku}}</option>
                                        @else
                                            <option value="{{$buku->id}} "> {{$buku->judul_buku}} </option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="mt-4 mb-0">
                                <input class="d-grid btn btn-primary btn-block " href="" value="Tambah" type="submit">
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
