<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>@yield('title')</title>
    <!-- header -->
    @include('layout._headerInclude')
</head>

<body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <!-- Navbar Brand-->
        <a class="navbar-brand ps-3" href="index.html">PERPUSTAKAAN</a>
        <!-- Sidebar Toggle-->
        <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i
                class="fas fa-bars"></i></button>
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <div class="sb-sidenav-menu-heading">Tabel</div>

                        <a class="nav-link" href="/home">
                            <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                            Mahasiswa
                        </a>
                        <a class="nav-link" href="/buku">
                            <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                            Buku
                        </a>
                        <a class="nav-link" href="/transaksi">
                            <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                            Transaksi
                        </a>
                    </div>
                </div>

            </nav>
        </div>
        <div id="layoutSidenav_content">
            <div>

                <!-- content -->
                @yield('main')

            </div>
        </div>
    </div>
    <!-- footer -->
    @include('layout._footerInclude')
</body>

</html>
