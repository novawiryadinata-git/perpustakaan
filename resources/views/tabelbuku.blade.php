@extends('layout.template')
@section('title','Tabel Buku')
@section('main')

            <div class="container-fluid px-4">
                <h1 class="mt-4">Buku</h1>

                <div class="card mb-4">
                    <div class="card-header d-flex align-items-center justify-content-between small">
                        <div>
                            <i class="fas fa-table me-1"></i>
                            Data Buku
                        </div>
                        <div>
                            <a href="/buku/create" class="btn btn-primary "><i class="fa fa-plus" aria-hidden="true"></i> Tambah</a>
                        </div>

                    </div>
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                                <tr>
                                    <th>Judul</th>
                                    <th>Pengarang</th>
                                    <th>Penerbit</th>
                                    <th>Tahun Terbit</th>
                                    <th>Tebal</th>
                                    <th>ISBN</th>
                                    <th>Stok</th>
                                    <th>Biaya Sewa Harian</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($buku as $bk)
                                <tr>
                                    <td> {{$bk->judul_buku}} </td>
                                    <td> {{$bk->pengarang}} </td>
                                    <td> {{$bk->penerbit}} </td>
                                    <td> {{$bk->tahun_terbit}} </td>
                                    <td> {{$bk->tebal}} </td>
                                    <td> {{$bk->isbn}} </td>
                                    <td> {{$bk->stok_buku}} </td>
                                    <td>Rp. {{$bk->biaya_sewa_harian}} </td>
                                    <td style="text-align: center;" class="card-header d-flex align-items-center justify-content-between small">
                                        <a href="/buku/editbuku/{{$bk->id}}" class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                        <a href="/buku/delete/{{$bk->id}}" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

@endsection
