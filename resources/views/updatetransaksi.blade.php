@extends('layout2.template')
@section('title', 'Edit Data Transaksi')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-7">
            <div class="card shadow-lg border-0 rounded-lg mt-5">
                <div class="card-header"><h3 class="text-center font-weight-light my-4">Edit Data Transaksi</h3></div>
                <div class="card-body">
                    @foreach ($transaksi as $trans)
                    <form action="" method="">
                        {{ csrf_field() }}
                        <div><input type="hidden" name="id_trans" value="{{ $trans->id }}"><br /></div>
                        <div class="form-floating mb-3">
                            <select class="selectpicker form-control" data-live-search="false"  name="mahasiswa" id="mahasiswa" required>
                                <option value="{{$trans->id_mahasiswa}}">{{$trans->nama}}</option>

                            </select>
                            <label>Nama Mahasiswa</label>
                        </div>
                        <div class="form-floating mb-3">
                            <select  class="selectpicker form-control" data-live-search="false" name="buku" id="buku" required>
                                <option value="{{$trans->id_buku}}">{{ $trans->judul_buku}}</option>

                            </select>
                            <label>Judul Buku</label>
                        </div>
                    </form>
                    @endforeach

                        @foreach ($transaksi as $trans)
                        <form action="/transaksi/updating-status" method="post">
                            {{ csrf_field() }}
                            <div><input type="hidden" name="id_trans" value=" {{$trans->id}} "><br/></div>
                            <div><input type="hidden" name="id_buku" value=" {{$trans->id_buku}} "><br/></div>

                            <div class="form-floating mb-3">
                                <select class="selectpicker form-control"  data-live-search="false"  class="form-control" name="status" id="status" required>
                                    <option value="{{$trans->status_pinjam}}">{{ $trans->status_pinjam}}</option>
                                    <option value="dikembalikan">dikembalikan</option>
                                </select>
                                <label>Status Pinjam</label>
                            </div>
                            <div class="form-floating mb-3 mb-md-0">
                                <select class="selectpicker form-control"  data-live-search="false"  name="tanggal_pinjam"  id="tgl_pinjam" class="form-control" required>
                                    <option value="{{$trans->tanggal_pinjam}}">{{ $trans->tanggal_pinjam}}</option>
                                </select>
                                <label>Tanggal Pinjam</label>
                            </div>
                            <br>

                            <div class="form-floating mb-3 mb-md-0 ">
                                <input id="tanggal_kembali" name="tanggal_kembali" class="form-control" type="date" placeholder="Tanggal Kembali" required autocomplete="tanggal_kembali" autofocus />
                                <label>Tanggal Kembali</label>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary">Update Status</button>
                        </form>
                        @endforeach

                </div>
            </div>
        </div>
    </div>
</div>


@endsection
