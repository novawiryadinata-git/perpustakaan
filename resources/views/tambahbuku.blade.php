@extends('layout2.template')
@section('title','Tambah Data Buku')
@section('content')


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="card shadow-lg border-0 rounded-lg mt-5">
                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Tambah Buku</h3></div>
                    <div class="card-body">
                        <form action="/buku/simpan" method="POST">
                            {{ csrf_field() }}
                            <div class="form-floating mb-3">
                                <input name="judul_buku" class="form-control" type="text" placeholder="Judul Buku" />
                                <label>Judul Buku</label>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6">
                                    <div class="form-floating mb-3 mb-md-0">
                                        <input name="pengarang" class="form-control" type="text" placeholder="Pengarang" />
                                        <label>Pengarang</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <input name="penerbit" class="form-control" id="inputLastName" type="text" placeholder="Email" />
                                        <label>Penerbit</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6">
                                    <div class="form-floating mb-3 mb-md-0">
                                        <input name="tahun_terbit" class="form-control" type="date" placeholder="Tahun Terbit" />
                                        <label>Tahun Terbit</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <input name="tebal" class="form-control" id="inputLastName" type="text" placeholder="Tebal" />
                                        <label >Tebal</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6">
                                    <div class="form-floating mb-3 mb-md-0">
                                        <input name="isbn" class="form-control" type="text" placeholder="ISBN" />
                                        <label>ISBN</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <input name="stok_buku" class="form-control" id="inputLastName" type="number" placeholder="Stok Buku" />
                                        <label>Stok Buku</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-floating mb-3">
                                <input name="biaya_sewa_harian" class="form-control" type="number" placeholder="Buaya Sewa Harian" />
                                <label>Biaya Sewa Harian</label>
                            </div>

                            <div class="mt-4 mb-0">
                                <input class="d-grid btn btn-primary btn-block " href="" value="Tambah" type="submit">

                                {{-- <div class="d-grid"><a class="btn btn-primary btn-block" href="login.html">Tambah Data</a></div> --}}
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection
